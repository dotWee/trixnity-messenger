package de.connect2x.trixnity.messenger.viewmodel.util

import kotlinx.coroutines.CoroutineDispatcher

expect val testMainDispatcher: CoroutineDispatcher